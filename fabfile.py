from fabric import Config
from fabric.tasks import task
from invoke.exceptions import UnexpectedExit
from invoke import run as local
from invoke import Responder
import random
import string
import os

def get_sudo_responder(c):
    return Responder(pattern=r'\[sudo\] password for .*:', response=(c.config.sudo.password + "\n"))

def dir_exists(c, d):
    try:
        result = c.run("ls %s" % d, hide=True)
    except UnexpectedExit:
        return False
    return True

def mkdir_if_needed(c, d):
    if not dir_exists(c, d):
        c.run("mkdir %s" % d)

def random_string(n):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=n))

@task
def prepare_build(c, repo, destination, debug=True, eclipse_files=False, eclipse_version="4.4", generator="Unix Makefiles"):
    """ Sets up a cmake build directory in the specified directory, which must exist on the remote system.

    repo (str): path to YottaDB source repo
    destination (str): where to place the cmake build directory
    debug (bool): if true, sets up a debug build
    eclipse_files (bool): if true, sets up files to project can be imported to eclipse
    eclipse_version (str): the version of eclipse to target if setting up eclipse files
    generator (str): generator to use for cmake
    """
    command = "cmake -DCMAKE_INSTALL_PREFIX:PATH=${PWD}"
    if debug:
        command += " -DCMAKE_BUILD_TYPE=Debug"
    if eclipse_files:
        command += " -G\"Eclipse CDT4 - %s\" -DCMAKE_ECLIPSE_VERSION=%s" % (generator, eclipse_version)
    else:
        command += " -G\"%s\"" % (generator)
    command += " " + repo
    mkdir_if_needed(c, destination)
    with c.cd(destination):
        c.run(command, pty=True)

@task
def build(c, folder, generator="Unix Makefiles", incremental=False):
    """ Invoke the make command on the folder
    """
    with c.cd(folder):
        if generator == "Ninja":
            c.run("ninja")
        else:
            c.run("make -j`grep -c ^processor /proc/cpuinfo`")

@task
def deploy_code_dir(c, repo, branch="HEAD", remote_repo_dir=""):
    """ Send the code from repo to the remote host
    """
    r_branch = random_string(16)
    if remote_repo_dir == "":
        remote_repo_dir = "/Distrib/YottaDB/" + version + "/"
    if not dir_exists(c, remote_repo_dir):
        c.run("git init %s" % remote_repo_dir)
    local("git -C {repo} push ssh://{user}@{host}:/{remote_repo_dir} {branch}:{r_branch}"
        .format(repo=repo, user=c.user, host=c.host, remote_repo_dir=remote_repo_dir, branch=branch, r_branch=r_branch))
    with c.cd(remote_repo_dir):
        c.run("git checkout %s" % r_branch)

@task
def remote_build(c, repo, version="V976", build_type="dbg", branch="HEAD", incremental=True, remote_repo_dir=""):
    """ Sends the code from repo to the remote host and runs cmake_build
    """
    if remote_repo_dir == "":
        remote_repo_dir = "/Distrib/YottaDB/" + version + "/"
    install_dir = os.path.join("/usr/library/", version, build_type)
    build_dir = os.path.join(remote_repo_dir, build_type)
    sudo_watcher = get_sudo_responder(c)
    deploy_code_dir(c, repo, branch, remote_repo_dir)
    if not incremental:
        c.sudo("rm -rf %s" % build_dir)
    if not dir_exists(c, build_dir):
        c.run("mkdir %s" % build_dir)
        prepare_build(c, remote_repo_dir, build_dir, debug=(build_type == "dbg"))
    if dir_exists(c, install_dir):
        c.sudo("rm -rf %s" % install_dir)
    build(c, build_dir)
    with c.cd(build_dir):
        c.run("sudo make install -j`grep -c ^processor /proc/cpuinfo`", pty=True, watchers=[sudo_watcher])
        with c.cd("yottadb_r123"):
            c.run("sudo ./ydbinstall --installdir={install_dir} --utf8 default --keep-obj --ucaseonly-utils --prompt-for-group --force-install"
                        .format(install_dir=install_dir)
                    , pty=True, watchers=[sudo_watcher])
    tdir = os.path.join("/tmp", "tmp." + random_string(16))
    c.run("mkdir %s" % tdir)
    with c.cd(tdir):
        c.run("tar xf {install_dir}/plugin/gtmcrypt/source.tar".format(install_dir=install_dir))
        with c.prefix("source /usr/library/gtm_test/ydb_cshrc.csh"):
            with c.prefix("source ~nars/utils/scripts/version.csh %s" % version):
                c.run("make", shell="/usr/local/bin/tcsh")
                c.run("setenv gtm_chset UTF-8 && setenv LC_ALL en_US.utf8 && sudo -E make install",
                        shell="/usr/local/bin/tcsh", pty=True, watchers=[sudo_watcher])
    c.run("rm -rf %s" % tdir)

@task
def create_build_dir(c, version):
    """ Create /Distrib/YottaDB/<Version> and give ownership to the user
    """
    path = os.path.join("/Distrib/YottaDB/", version)
    if dir_exists(c, path):
        c.sudo("rm -rf %s" % path)
    c.sudo("git init %s" % path)
    c.sudo("chown -R %s %s" % (c.user, path))

@task
def deploy_test_dir(c, repo, version="T976", branch="HEAD"):
    remote_repo_dir = os.path.join("/usr/library/gtm_test/", version)
    r_branch = random_string(16)
    if not dir_exists(c, remote_repo_dir):
        c.sudo("git init %s" % remote_repo_dir)
        c.sudo("chown -R %s %s" % (c.user, remote_repo_dir))
    local("git -C {repo} push ssh://{user}@{host}:/{remote_repo_dir} {branch}:{r_branch}"
        .format(repo=repo, user=c.user, host=c.host, remote_repo_dir=remote_repo_dir, branch=branch, r_branch=r_branch))
    with c.cd(remote_repo_dir):
        c.run("git checkout %s" % r_branch)

@task
def run_test(c, test, subtest=None, test_version="T976", out1=None, out2=None, mail=None, version="V976",
        foreground=True, test_options="-testtiminglog -failsubtestmail -noencrypt"):
    if not out1:
        out1 = "/testarea1/%s/" % c.user
    if not out2:
        out2 = "/testarea2/%s/" % c.user
    if not mail:
        mail = "%s@yottadb.com" % c.user
    command = os.path.join("/usr/library/gtm_test", test_version, "com/gtmtest.csh")
    command += " -o " + out1 + " -ro " + out2
    command += " -ml " + mail
    command += " " + test_options
    if foreground:
        command += " -fg"
    command += " -t " + test
    if subtest:
        command += " -st " + subtest
    with c.prefix("source /usr/library/gtm_test/ydb_cshrc.csh"):
        with c.prefix("source ~nars/utils/scripts/version.csh %s" % version):
            c.run(command, shell="/usr/local/bin/tcsh")

@task
def d_all(c, test_options="", test_version="T976", version="V976", out1=None, out2=None, mail=None):
    if not out1:
        out1 = "/testarea1/%s/" % c.user
    if not out2:
        out2 = "/testarea2/%s/" % c.user
    if not mail:
        mail = "%s@yottadb.com" % c.user
    command = os.path.join("/usr/library/gtm_test", test_version, "com/gtmtest.csh")
    command += " -E_ALL -failmail -noencrypt -env eall_noinverse=1 " + test_options
    command += " -o " + out1 + " -ro " + out2
    command += " -ml " + mail
    command += " " + test_options
    command += "  -distributed dall:orval"
    with c.prefix("source /usr/library/gtm_test/ydb_cshrc.csh"):
        with c.prefix("source ~nars/utils/scripts/version.csh %s" % version):
            c.run(command, shell="/usr/local/bin/tcsh")

@task
def kill_tests(c):
    command = "ps x | grep -E \"(com|mumps|mupip|T976)\" | grep -v 'ps x' | awk '{print $1'} | xargs kill -9"
    try:
        c.run(command)
    except Exception:
        pass
